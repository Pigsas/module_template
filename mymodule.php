<?php

use Jugeras\MyModule\Install\Installer;
use Jugeras\MyModule\Install\Uninstaller;

class MyModule extends Module
{

    /**
     * @var array
     */
    private $config;

    public function __construct()
    {
        $this->tab = 'other_modules';
        $this->name = 'mymodule';
        $this->version = '1.0.0';
        $this->author = 'Jugeras';
        $this->bootstrap = true;

        parent::__construct();
        $this->autoload();
        $this->config = [
            'hooks' => [

            ]
        ];

        $this->displayName = $this->l('');
        $this->description = $this->l('');
    }

    public function install()
    {
        $installer = new Installer($this, $this->config);
        return parent::install() && $installer->initInstaller();
    }

    public function uninstall()
    {
        $uninstaller = new Uninstaller($this, $this->config);
        return parent::uninstall()&& $uninstaller->initUninstaller();
    }

    private function autoLoad()
    {
        $autoLoadPath = $this->getLocalPath().'vendor/autoload.php';

        require_once $autoLoadPath;
    }
}
