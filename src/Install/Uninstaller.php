<?php

namespace Jugeras\MyModule\Install;
use Exception;
use Jug_SpecialOffers;

class Uninstaller
{
    /**
     * @var MyModule
     */
    private $module;
    /**
     * @var array
     */
    private $config;

    public function __construct(MyModule $module, array $config)
    {
        $this->module = $module;
        $this->config = $config;
    }
    public function initUninstaller()
    {
        if(!$this->uninstallHooks($this->config['hooks'])) {
            return false;
        }
        return true;
    }

    private function uninstallHooks(array $hooks): bool
    {

        foreach ($hooks as $hook) {
            if(!$this->module->unregisterHook($hook)){
                throw new Exception(
                    sprintf(
                        $this->module->l('Hook %s has not been unregistered.'),
                        $hook
                    )
                );
            }
        }

        return true;
    }
}