<?php

namespace Jugeras\MyModule\Install;
use Exception;
use Jug_SpecialOffers;

class Installer
{
    /**
     * @var MyModule
     */
    private $module;
    /**
     * @var array
     */
    private $config;

    public function __construct(MyModule $module, array $config)
    {
        $this->module = $module;
        $this->config = $config;
    }
    public function initInstaller()
    {
        if(!$this->installHooks($this->config['hooks'])) {
            return false;
        }
        return true;
    }

    private function installHooks(array $hooks): bool
    {

        foreach ($hooks as $hook) {
            if(!$this->module->registerHook($hook)){
                throw new Exception(
                    sprintf(
                        $this->module->l('Hook %s has not been installed.'),
                        $hook
                    )
                );
            }
        }

        return true;
    }
}